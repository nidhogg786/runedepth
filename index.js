const axios = require('axios');
const express = require('express');
var path = require('path');
var cors = require('cors');
const app = express();
const port = 3000;
let RUNE = {};
let BITMAX__RUNE_USD = {};
let BINANCE_RUNE_BNB = {};
let BINANCE_RUNE_USD = {};
let BINANCE_RUNE_BUSD = {};

const getData = async () => {
  try {
    const runeBNB = axios.get(
      'https://dex.binance.org/api/v1/depth?symbol=RUNE-B1A_BNB&limit=1000'
    );
    const runeUSD = axios.get(
      'https://bitmax.io/api/v1/depth?symbol=RUNE-USDT&n=10000'
    );
    const bnbUSD = axios.get(
      'https://dex.binance.org/api/v1/ticker/24hr?symbol=BNB_BUSD-BD1'
    );
    const runeBUSD = axios.get(
      'https://dex.binance.org/api/v1/depth?symbol=RUNE-B1A_BUSD-BD1&limit=1000'
    );
    const markets = await Promise.all([runeBNB, runeUSD, bnbUSD, runeBUSD]);
    let bnbprice = Number(markets[2].data[0].weightedAvgPrice);

    ///////////////////////
    const calculateBinanceRuneUsd = async () => {
      try {
        BINANCE_RUNE_USD = {
          bids: markets[0].data.bids.map((x) => [
            `${Number(x[0] * bnbprice).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
          asks: markets[0].data.asks.map((x) => [
            `${Number(x[0] * bnbprice).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
        };

        BINANCE_RUNE_USD.bids = BINANCE_RUNE_USD.bids.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_USD.asks = BINANCE_RUNE_USD.asks.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_USD.bids = await condense(BINANCE_RUNE_USD.bids);
        BINANCE_RUNE_USD.asks = await condense(BINANCE_RUNE_USD.asks);
      } catch (error) {
        throw error;
      }
    };

    await calculateBinanceRuneUsd();

    //////////////////////////////////////////////////
    const calculateBinanceRuneBnb = async () => {
      try {
        BINANCE_RUNE_BNB = {
          bids: markets[0].data.bids.map((x) => [
            `${Number(x[0]).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
          asks: markets[0].data.asks.map((x) => [
            `${Number(x[0]).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
        };

        BINANCE_RUNE_BNB.bids = BINANCE_RUNE_BNB.bids.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_BNB.asks = BINANCE_RUNE_BNB.asks.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_BNB.bids = await condense(BINANCE_RUNE_BNB.bids);
        BINANCE_RUNE_BNB.asks = await condense(BINANCE_RUNE_BNB.asks);
      } catch (error) {
        throw error;
      }
    };
    await calculateBinanceRuneBnb();
    ////////////////////////////////////////////////////

    //////////////////////////////////////////////////
    const calculateBitMaxRuneUsd = async () => {
      try {
        BITMAX__RUNE_USD = {
          bids: markets[1].data.bids.map((x) => [
            `${Number(x[0]).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
          asks: markets[1].data.asks.map((x) => [
            `${Number(x[0]).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
        };

        BITMAX__RUNE_USD.bids = BITMAX__RUNE_USD.bids.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BITMAX__RUNE_USD.asks = BITMAX__RUNE_USD.asks.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BITMAX__RUNE_USD.bids = await condense(BITMAX__RUNE_USD.bids);
        BITMAX__RUNE_USD.asks = await condense(BITMAX__RUNE_USD.asks);
      } catch (error) {
        throw error;
      }
    };
    await calculateBitMaxRuneUsd();
    ////////////////////////////////////////////////////

    ////////////////////////////////////////
    const calculateRuneBusd = async () => {
      try {
        BINANCE_RUNE_BUSD = {
          bids: markets[3].data.bids.map((x) => [
            `${Number(x[0] * bnbprice).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
          asks: markets[3].data.asks.map((x) => [
            `${Number(x[0] * bnbprice).toFixed(3)}`,
            `${Number(x[1]).toFixed(0)}`,
          ]),
        };

        BINANCE_RUNE_BUSD.bids = BINANCE_RUNE_BUSD.bids.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_BUSD.asks = BINANCE_RUNE_BUSD.asks.map((x) => [
          Number(x[0]),
          Number(x[1]),
        ]);
        BINANCE_RUNE_BUSD.bids = await condense(BINANCE_RUNE_BUSD.bids);
        BINANCE_RUNE_BUSD.asks = await condense(BINANCE_RUNE_BUSD.asks);
      } catch (error) {
        throw error;
      }
    };
    await calculateRuneBusd();
    ////////////////////////////////////////

    const calculateRune = async () => {
      try {
        let allbids = [];
        let allasks = [];

        for (let bid of BINANCE_RUNE_USD.bids) {
          allbids.push(bid);
        }

        for (let bid of BITMAX__RUNE_USD.bids) {
          allbids.push(bid);
        }

        for (let ask of BINANCE_RUNE_USD.asks) {
          allasks.push(ask);
        }

        for (let ask of BITMAX__RUNE_USD.asks) {
          allasks.push(ask);
        }

        allasks = allasks.map((x) => [Number(x[0]), Number(x[1])]);
        allbids = allbids.map((x) => [Number(x[0]), Number(x[1])]);

        let finalbids = await condense(allbids);
        let finalasks = await condense(allasks);
        finalbids = finalbids.map((x) => [`${x[0]}`, `${x[1]}`]);
        finalasks = finalasks.map((x) => [`${x[0]}`, `${x[1]}`]);
        let final = {
          bids: finalbids,
          asks: finalasks,
        };
        console.log(
          `DATA: ${Date.now()} BIDS: ${final.bids.length} ASKS: ${
            final.asks.length
          }`
        );
        return final;
      } catch (error) {
        throw error;
      }
    };

    RUNE = await calculateRune();
  } catch (e) {
    console.log(e);
  }
};

const condense = async (array) => {
  let newArray = [];
  for await (let x of array) {
    let found = false;
    for await (let y of newArray) {
      if (y[0] == x[0]) {
        y[1] = x[1] + y[1];
        found = true;
      }
    }
    if (!found) {
      newArray.push([x[0], x[1]]);
    }
  }
  return newArray;
};

app.use(cors());
app.use(express.static(path.join(__dirname, 'build')));

app.get('/api-combined', async (req, res) => {
  res.setHeader('Content-Type', 'text/json');
  await getData();
  res.send(RUNE);
});

app.get('/api-bitmax_rune_usd', async (req, res) => {
  res.setHeader('Content-Type', 'text/json');
  await getData();
  res.send(BITMAX__RUNE_USD);
});

app.get('/api-binance_rune_bnb', async (req, res) => {
  res.setHeader('Content-Type', 'text/json');
  await getData();
  res.send(BINANCE_RUNE_BNB);
});

app.get('/api-binance_rune_busd', async (req, res) => {
  res.setHeader('Content-Type', 'text/json');
  await getData();
  res.send(BINANCE_RUNE_BUSD);
});

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// app.listen(port, () => console.log(`Example app listening on port ${port}!`));
app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`)
);

getData();
